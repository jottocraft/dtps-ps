## Power+
[![Power+ Release](https://img.shields.io/github/release/jottocraft/dtps.svg)](https://github.com/jottocraft/dtps/releases)
[![Website Status](https://img.shields.io/website/https/dtps.js.org.svg?label=Power%2B)](https://dtps.js.org)
[![Power+ Chrome Web Store Rating](https://img.shields.io/chrome-web-store/stars/pakgdifknldaiglefmpkkgfjndemfapo.svg)](https://chrome.google.com/webstore/detail/power%20/pakgdifknldaiglefmpkkgfjndemfapo/reviews)

For users looking to install Power+, visit the Power+ website by clicking [here](https://dtps.js.org)

Help test Power+ for Canvas by following the instructions [here](https://dtps.js.org/canvas)

For Power+ status, visit the [jottocraft.com status page](https://status.jottocraft.com)

<br />

#### Power+ Roadmap
Here's how Power+ will be changing in the coming months. This rollout plan is constantly being updated as new information becomes available. (Updated 4/26/2019):
* ~~April 10th, 2019: Power+ for Canvas can be tested by anyone ([install instructions](https://dtps.js.org/canvas))~~
* June 2019: Power+ for PowerSchool will no longer recive bugfix updates
* July 2019: Power+ for PowerSchool will be archived and will no longer be supported. Power+ for Canvas will officially become Power+ and will be hosted on the main Power+ repository. At this point, Power+ for Canvas will become the version of Power+ and will be enabled by default for everyone.
* Fall 2019: Power+ for Canvas will be released as a standalone website (the script will be updated to notify users of the change). Power+ development will continue as usual with stable and dev channels. This is currently being tested internally and will hopefully not be in beta for very long, but needs admin to add it as an app in Canvas for it to work.

#### Versions of Power+
* Power+ for PowerSchool (Power+)
* Power+ for Canvas
* Power+ Standalone

<br /><br />

#### Loader scripts

[bookmark script](https://dtps.js.org/bookmark.txt)

or

[chrome extension (auto load)](https://chrome.google.com/webstore/detail/power%20/pakgdifknldaiglefmpkkgfjndemfapo)

or

[dev bookmark script (unstable)](https://dtps.js.org/devbookmark.txt)

[firefox script (unsupported)](https://pastebin.com/raw/6Nh6sABu)

<br /><br /><br /><br /><br /><br />

(c) 2018-2019 jottocraft &nbsp;&nbsp; [license](https://github.com/jottocraft/dtps/blob/master/LICENSE)
